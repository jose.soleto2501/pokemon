package ex23_Pokemon;
import java.util.Scanner;

public class PokemonTest {
    public static String PATH;
    public static String FILE = "pokemons.csv";
    public static String FILE_TOTALS;
    public static String FILE_SEARCH;
    public static String FILE_GENERATION;
    public static String FILE_LEGENDARY;
    public static String FILE_GEN_NAME;
    public static String FILE_TYPE;
    public static String FILE_EXT;


    public static void main(String[] args) {
        FileManager fileManager = new FileManager();
        System.out.println("BIENVENIDO A POKECOSITAS CHULAS :)");
        byte opcion = mostrarMenu();
        switch (opcion) {
            case 1:
                fileManager.readTotals();
                break;
            case 2:
                fileManager.pokemonSearch();
                break;
            case 3:
                fileManager.extractGeneration();
                break;
            case 4:
                fileManager.orderLegendary();
                break;
            case 5:
                fileManager.orderByType();
                break;
            case 6:
                fileManager.orderByGenAndName();
                break;
            default:
                System.out.println("This option dont exist, choose an enable option");

        }
    }
    public static void dataEntryByte(){

    }


    private static byte mostrarMenu() {
        byte opcion;
        Scanner sc = new Scanner(System.in);
        System.out.println("MENU");
        System.out.println("1. TOTALS");
        System.out.println("2. POKEMON SEARCH");
        System.out.println("3. EXTRACT GENERATION");
        System.out.println("4. ORDER LEGENDARY");
        System.out.println("5. ORDER BY TYPE1 AND TYPE2");
        System.out.println("6. ORDER BY GENERATION AND NAME");
        System.out.println("O. QUIT");
        System.out.println("CHOOSE MENU OPTION: ");
        opcion= sc.nextByte();

        return opcion;
    }
}
